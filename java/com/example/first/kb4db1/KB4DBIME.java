package com.example.first.kb4db1;

import android.content.Context;
import android.content.SharedPreferences;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Button;

import static android.inputmethodservice.Keyboard.KEYCODE_DELETE;
import static android.inputmethodservice.Keyboard.KEYCODE_SHIFT;

public class KB4DBIME extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener
{
    private final MorseCodeTranslator translator = new MorseCodeTranslator();
    private KeyboardView kv;
    private Keyboard keyboard;
    private Vibrator vib;
    private boolean caps = false;
    private StringBuilder text = new StringBuilder(0);
    private MorseCodeKeyboard morseCodeKeyboard = null;
    private TTSmanager tts=null;
    private boolean vibrationEnabled = true;
    private byte vibrateLength = (byte) 200;


    public void onCreate()
    {
    super.onCreate();
    vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
     }
    @Override public void onInitializeInterface() {
        morseCodeKeyboard = new MorseCodeKeyboard(this, R.xml.qwerty);
    }

    @Override
    public View onCreateInputView() {
        kv = (KeyboardView)getLayoutInflater().inflate(R.layout.keyboard, null);
        kv.setOnKeyboardActionListener(this);
        keyboard = new Keyboard(this, R.xml.qwerty);
        kv.setKeyboard(morseCodeKeyboard);
        return kv;
    }

    @Override public void onStartInput(EditorInfo attribute, boolean restarting) {
// get the user's desired preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        vibrationEnabled = prefs.getBoolean("allow_vibrate",false);
        vibrateLength = (byte) Integer.parseInt(prefs.getString("vibrate_length", "200"));
    }

    private void playClick(int keyCode){
        AudioManager am = (AudioManager)getSystemService(AUDIO_SERVICE);
        switch(keyCode){
            case 32:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);

                break;
            case Keyboard.KEYCODE_DONE:
            case 10:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case Keyboard.KEYCODE_DELETE:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default: am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
        }
    }


    @Override
    public void onKey(int primaryCode, int[] keyCodes) {

        InputConnection ic = getCurrentInputConnection();
        playClick(primaryCode);
        switch(primaryCode)
        {
            case 1:
                text.append(".");

                break;
            case 2:
                text.append("-");

                break;

            case 3:
                handelspeak();
                break;

            case Keyboard.KEYCODE_DELETE :

                ic.deleteSurroundingText(1, 0);
                break;
            case KEYCODE_SHIFT:
                caps = !caps;
                keyboard.setShifted(caps);
                kv.invalidateAllKeys();
                break;
            case Keyboard.KEYCODE_DONE:

                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;
            default:
                char code = (char)primaryCode;
                if(Character.isLetter(code) && caps){
                    code = Character.toUpperCase(code);
                }
                ic.commitText(String.valueOf(code),1);
        }
    }

    private void handelspeak() {
        InputConnection ic = getCurrentInputConnection();
        if(ic.getTextBeforeCursor(1,0).toString().length() > 0){

          final String text = ic.getTextBeforeCursor(1,0).toString();
            tts.InitQueue(text);

        }
        vib.vibrate(100);
    }




    private void commitText(String s) {
        InputConnection ic = getCurrentInputConnection();
        ic.commitText(s, /* new cursor pos */1);
        text.setLength(0);
    }

    @Override
    public void onPress(int primaryCode) {

        if (vibrationEnabled) {
            vib.vibrate(100);
        }
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeDown() {
    }

    @Override
    public void swipeLeft() {
    }

    @Override
    public void swipeRight() {
    }

    @Override
    public void swipeUp() {
    }


    private void commitText(boolean uppercase) {
        if (text.length() <= 0) return;
        InputConnection ic = getCurrentInputConnection();
// check for prosigns first and foremost
        if (translator.fromMorse(text.toString(), false) == "Newline") {
            ic.sendKeyEvent( new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER) );
        }
        else if (translator.fromMorse(text.toString(), false) == "EOT") {
// perform the editor action as determined by the editor itself
            ic.performEditorAction(EditorInfo.IME_MASK_ACTION | EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        }
        else {
// if the composed dots and dashed do not represent a procedural signal
            ic.commitText(translator.fromMorse(text.toString(), uppercase), /* new cursor pos */ 1);
        }
        text.setLength(0);
    }
    private void handleClose() {
        requestHideSelf(0);
        //KeyboardView.closing();
    }
}